const burger = document.querySelector('.header__burger');
const menu = document.querySelector('.header__menu');
const show = function() {
    burger.classList.toggle("header__burger--active")
    menu.classList.toggle("header__menu--active")
}

burger.addEventListener('click', show);


