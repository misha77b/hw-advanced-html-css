const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const gulpCleanCss = require('gulp-clean-css');
const gulpClean = require('gulp-clean');
const concat = require('gulp-concat');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');


const copyHtml = function() {
    return gulp.src("src/**/*.html")
        .pipe(gulp.dest("./dist"));

}

const scss = function() {
    return gulp.src('./src/**/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(concat('styles.min.css'))
        .pipe(gulpCleanCss({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/scss'))
        .pipe(browserSync.stream());
}

const jsMin = function() {
    return gulp.src('./src/**/*.js')
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
}

const imgMin = function() {
    return gulp.src('./src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'))
}

gulp.task('watch', function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/img/*', imgMin)
    gulp.watch('./src/**/*.scss', scss);
    gulp.watch("./src/*.html").on('change', browserSync.reload);
    gulp.watch("./src/*.html", copyHtml);
    gulp.watch('./src/**/*.js', jsMin);
})


gulp.task('clean', function() {
    return gulp.src('./dist/*')
        .pipe(gulpClean())
});

gulp.task('copyHtml', copyHtml)
gulp.task('scss', scss)
gulp.task('jsMin', jsMin)
gulp.task('imgMin', imgMin)

gulp.task('build', gulp.series('clean', gulp.parallel('copyHtml', 'scss', 'jsMin', 'imgMin')))
gulp.task('dev', gulp.series('build', 'watch'))