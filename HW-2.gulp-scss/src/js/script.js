const menuBtn = document.querySelector('.header__burger-icon');
const menuList = document.querySelector('.navigation__nav-menu')
let menuOpen = false;
menuBtn.addEventListener('click', () => {
    if (!menuOpen) {
        menuBtn.classList.add('show');
        menuList.classList.add('active');
        menuOpen = true;
    } else {
        menuBtn.classList.remove('show');
        menuList.classList.remove('active');
        menuOpen = false;
    }
})